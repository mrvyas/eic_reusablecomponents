import {cleanup, render} from '@testing-library/react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import {Indicator} from '../src/CustomizedLoader';

let Platform;
beforeEach(() => {
  Platform = require('react-native').Platform;
});

describe('Indicator component test', () => {
  const props = {
    animating: true,
  };
  const stopAnimation = jest.fn();
  const resumeAnimation = jest.fn();

  test('Indicator start', () => {
    jest.useFakeTimers();

    const wrapper = renderer.create(<Indicator />);
    expect(wrapper).toMatchSnapshot();

    jest.runAllTimers();
    jest.useRealTimers();
  });

  test('Indicator stop and resume', async () => {
    jest.useFakeTimers();

    const {update} = render(<Indicator {...props} />);

    const newPropsStopAnimation = {...props, animating: false};
    update(<Indicator {...newPropsStopAnimation} />);

    const newPropsResumeAnimation = {...props, animating: true};
    update(<Indicator {...newPropsResumeAnimation} />);

    expect(stopAnimation).toBeTruthy();
    expect(resumeAnimation).toBeTruthy();

    jest.runAllTimers();
    jest.useRealTimers();
  });
});

describe('Indicator component start function test for platform web', () => {
  const props = {
    animating: true,
  };

  beforeEach(() => {
    Platform.OS = 'web';
  });

  afterAll(cleanup);

  test('Indicator start', () => {
    const wrapper = renderer.create(<Indicator {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('Indicator compoennt stop and resume function test for platform web', () => {
  const props = {
    animating: false,
  };
  const stopAnimation = jest.fn();
  const resumeAnimation = jest.fn();

  beforeEach(() => {
    Platform.OS = 'web';
  });
  afterAll(cleanup);

  test('Indicator stop and resume for web', async () => {
    jest.useFakeTimers();

    const {update} = render(<Indicator {...props} />);

    const newPropsResumeAnimation = {...props, animating: true};
    update(<Indicator {...newPropsResumeAnimation} />);

    const newPropsStopAnimation = {...props, animating: false};
    update(<Indicator {...newPropsStopAnimation} />);

    expect(stopAnimation).toBeTruthy();
    expect(resumeAnimation).toBeTruthy();

    jest.runAllTimers();
    jest.useRealTimers();
  });
});
