import React from 'react';
import renderer from 'react-test-renderer';
import {UIActivityIndicator} from '../src/CustomizedLoader';

describe('UI Activity indicator ', () => {
  test('UI Activity indicator snapshot', () => {
    const wrapper = renderer.create(<UIActivityIndicator />);
    expect(wrapper).toMatchSnapshot();
  });
});
