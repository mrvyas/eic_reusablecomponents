import React from 'react';
import renderer from 'react-test-renderer';
import LoaderComponent from '..';

describe('Loader', () => {
  test('Basic Loader', () => {
    const wrapper = renderer.create(<LoaderComponent />);
    expect(wrapper).toMatchSnapshot();
  });
  test('Loader with title', () => {
    const wrapper = renderer.create(
      <LoaderComponent
        activityIndicatorType="Default"
        showTitle={true}
        titleText="Loading"
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Loader with subtitle', () => {
    const wrapper = renderer.create(
      <LoaderComponent
        activityIndicatorType="Default"
        titleText="Loading"
        subTitleText="Please Wait"
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Ball Indicator', () => {
    const wrapper = renderer.create(
      <LoaderComponent activityIndicatorType="BallIndicator" />,
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Material Indicator', () => {
    const wrapper = renderer.create(
      <LoaderComponent activityIndicatorType="MaterialIndicator" />,
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Skype Indicator', () => {
    const wrapper = renderer.create(
      <LoaderComponent activityIndicatorType="SkypeIndicator" />,
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('UIActivity Indicator', () => {
    const wrapper = renderer.create(
      <LoaderComponent activityIndicatorType="UIActivityIndicator" />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
