import React from 'react';
import renderer from 'react-test-renderer';
import {BallIndicator} from '../src/CustomizedLoader';

describe('Ball indicator ', () => {
  test('Ball indicator snapshot', () => {
    const wrapper = renderer.create(<BallIndicator />);
    expect(wrapper).toMatchSnapshot();
  });
});
