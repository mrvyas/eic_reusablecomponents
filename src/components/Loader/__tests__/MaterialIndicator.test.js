import React from 'react';
import renderer from 'react-test-renderer';
import {MaterialIndicator} from '../src/CustomizedLoader';

describe('Material indicator ', () => {
  test('Material indicator snapshot', () => {
    const wrapper = renderer.create(<MaterialIndicator />);
    expect(wrapper).toMatchSnapshot();
  });
});
