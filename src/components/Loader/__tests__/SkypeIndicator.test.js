import React from 'react';
import renderer from 'react-test-renderer';
import {SkypeIndicator} from '../src/CustomizedLoader';

describe('Skype indicator ', () => {
  test('Skype indicator snapshot', () => {
    const wrapper = renderer.create(<SkypeIndicator />);
    expect(wrapper).toMatchSnapshot();
  });
});
