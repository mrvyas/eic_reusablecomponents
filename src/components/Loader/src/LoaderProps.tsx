import {ColorValue, StyleProp, TextStyle} from 'react-native';

export interface ActivityIndicatorProps {
  /**
   * Size of the indicator. Small has a height of 20, large has a height of 36.
   */
  size?: number;

  /**
   * The foreground color of the spinner (default is gray).
   */
  color?: string;

  /**
   * Count for animation styles for customized loader
   *
   * For BallIndicator count value is 8
   *
   * For MaterialIndicator count value is 8
   *
   * For SkypeIndicator count value is 5
   *
   * For UIActivityIndicator count value is 12
   */
  count?: number;

  /**
   * minScale props is used for SkypeIndicator animation. Default value is 0.2
   */
  minScale?: number;

  /**
   * maxScale props is used for SkypeIndicator animation. Default value is 1.0
   */
  maxScale?: number;
}

export interface BackGroundStyleProps {
  /**
   * Specify width for background style.
   */
  width?: number;

  /**
   * Specify height for background style.
   */
  height?: number;

  /**
   * Specify borderRadius (curve) for borders.
   */
  borderRadius?: number;

  /**
   * Background color for the loader
   */
  backgroundColor?: ColorValue;
}
export interface LoaderProps {
  /**
   * Use when we want to add customized properties for Activity indicator
   *
   * Default value for size is 30 and color value is grey
   */
  activityIndicator?: ActivityIndicatorProps | undefined;

  /**
   * Used when we want to specify the background styles for Loader
   */
  backgroundStyle?: StyleProp<BackGroundStyleProps>;

  /**
   * Specify title for loader component. If value is given, title would be displayed below loader
   */
  titleText?: string;

  /**
   * Specify subtitle for loader component. If value is given, sub title would be displayed below loader
   */
  subTitleText?: string;

  /**
   * Styles for title text.
   */
  titleTextStyle?: StyleProp<TextStyle>;

  /**
   * Styles for subtitle text.
   */
  subTitleTextStyle?: StyleProp<TextStyle>;

  /**
   * Select type of loader weather it is Default | BallIndicator | MaterialIndicator |
   * SkypeIndicator | UIActivityIndicator
   *
   * Default type is Basic
   */
  activityIndicatorType?:
    | 'Basic'
    | 'BallIndicator'
    | 'MaterialIndicator'
    | 'SkypeIndicator'
    | 'UIActivityIndicator';
}
