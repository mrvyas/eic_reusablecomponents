import React, {memo} from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';

import {
  color,
  commonStyle,
  getTestID,
  testIds,
} from '@eic-reusable-component/shared';
import {normalizeValue} from '@eic-reusable-component/shared/utils';
import {
  BallIndicator,
  MaterialIndicator,
  SkypeIndicator,
  UIActivityIndicator,
} from './CustomizedLoader';
import {LoaderProps} from './LoaderProps';

const LoaderComponent = ({
  titleText,
  subTitleText,
  activityIndicator = {
    size: normalizeValue(30),
    color: color.grey,
  },
  backgroundStyle,
  activityIndicatorType = 'Basic',
  titleTextStyle,
  subTitleTextStyle,
}: LoaderProps) => {
  return (
    <View style={[styles.loaderStyle, backgroundStyle]}>
      {activityIndicatorType === 'Basic' && (
        <ActivityIndicator
          animating={true}
          size={activityIndicator?.size}
          color={activityIndicator?.color}
          {...getTestID(testIds.basicLoader)}
        />
      )}
      <View style={styles.container}>
        {activityIndicatorType === 'BallIndicator' && (
          <BallIndicator
            color={activityIndicator?.color}
            count={activityIndicator?.count}
            size={activityIndicator?.size}
            {...getTestID(testIds.ballIndicator)}
          />
        )}
        {activityIndicatorType === 'MaterialIndicator' && (
          <MaterialIndicator
            color={activityIndicator?.color}
            count={activityIndicator?.count}
            size={activityIndicator?.size}
            {...getTestID(testIds.materialIndicator)}
          />
        )}
        {activityIndicatorType === 'SkypeIndicator' && (
          <SkypeIndicator
            color={activityIndicator?.color}
            count={activityIndicator?.count}
            size={activityIndicator?.size}
            minScale={activityIndicator?.minScale}
            maxScale={activityIndicator?.maxScale}
            {...getTestID(testIds.skypeIndicator)}
          />
        )}
        {activityIndicatorType === 'UIActivityIndicator' && (
          <UIActivityIndicator
            color={activityIndicator?.color}
            count={activityIndicator?.count}
            size={activityIndicator?.size}
            {...getTestID(testIds.uiActivityIndicator)}
          />
        )}
      </View>
      {titleText && (
        <Text style={[styles.title, titleTextStyle]}>{titleText}</Text>
      )}
      {subTitleText && (
        <Text style={[styles.subTitle, subTitleTextStyle]}>{subTitleText}</Text>
      )}
    </View>
  );
};

export default memo(LoaderComponent);

const styles = StyleSheet.create({
  container: {
    ...commonStyle.flexRow,
    ...commonStyle.justifyCenter,
    ...commonStyle.alignSelfCenter,
  },
  loaderStyle: {
    ...commonStyle.alignSelfCenter,
    ...commonStyle.justifyCenter,
  },
  title: {
    ...commonStyle.alignSelfCenter,
    fontSize: normalizeValue(14),
    marginTop: normalizeValue(5),
    color: color.black,
  },
  subTitle: {
    ...commonStyle.alignSelfCenter,
    fontSize: normalizeValue(12),
    color: color.grey,
  },
});
