import {LoaderProps} from '../../Loader/src/LoaderProps';
import {
  ImageSourcePropType,
  ImageStyle,
  StyleProp,
  TextStyle,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';

export interface ButtonLoaderProps extends LoaderProps {
  /**
   * Use this for the change the position of the loader in the button
   *
   * default the loader will be render in the middle of the button by replace the button title
   * @transparent Loader will be render on the button by making transparent the button partially
   * @swipeLeft Loader will be render on the left side of the title
   * @swipeRight Loader will be render on the right side of the title
   * @left Loader will be render on the left corder of the button
   * @right Loader will be render on the right corder of the button
   */
  type:
    | 'default'
    | 'transparent'
    | 'swipeLeft'
    | 'swipeRight'
    | 'left'
    | 'right'
    | undefined;
}

export interface ImageProps {
  /**
   * Use this if you wants to add custom image in the button
   */
  Source?: ImageSourcePropType | undefined;

  /**
   * Use this whenever wants to change the style of the image
   */
  Style?: StyleProp<ImageStyle>;

  /**
   * Use this for change the potion of the image in the button with the text
   * @swipeLeft Image will be on the left side of the text
   * @swipeRight Image will be on the right side of the text
   * @right Image will be on the right corner of the button
   * @left Image will be on the left corner of the button
   */
  type?: 'swipeLeft' | 'swipeRight' | 'left' | 'right' | undefined;
}

export interface ButtonProps {
  /**
   * Use for change any style of the main button view
   */
  containerStyle?: StyleProp<ViewStyle>;

  /**
   * Use this for pass any touchable props for the components
   */
  touchableProps?: TouchableOpacityProps;

  /**
   * Use this for change the background of the button
   * or you can add in the 'containerStyle' as the style param
   */
  backgroundColor?: string;

  /**
   * Use this for add the image in the button (with text or without text)
   */
  imageProps?: ImageProps | undefined;

  /**
   * Use this for the main Title text of the button
   */
  titleText?: string;

  /**
   * Use this for give the style to the view of the title text
   */
  titleTextViewStyle?: StyleProp<ViewStyle>;

  /**
   * Use to give custom style of the main Title text
   */
  titleTextStyle?: StyleProp<TextStyle>;

  /**
   * When this is true it show the loader in the button
   *
   * There are multiple type of the loader you can you loaderProps for change the loader
   */
  loaderShow?: boolean;

  /**
   * Use this for change he loader as well as the position of ghe loader
   */
  loaderProps?: ButtonLoaderProps | undefined;

  /**
   * This is the onPress event of the button
   * @returns
   */
  onPress?: () => void;

  /**
   * When true, inform user that button is disable and user can't able to click on that
   */
  disabled?: boolean;
}
