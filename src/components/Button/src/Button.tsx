import React, {memo, useMemo} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import LoaderComponent from '../../Loader';
import {
  color,
  commonStyle,
  getTestID,
  sizes,
  testIds,
} from '@eic-reusable-component/shared';
import {normalizeValue} from '@eic-reusable-component/shared/utils';
import {ButtonProps} from './ButtonProps';

const ButtonComponent = ({
  containerStyle = {},
  backgroundColor,
  imageProps = {},
  disabled = false,
  titleText,
  titleTextViewStyle = {},
  titleTextStyle = {},
  loaderShow = false,
  loaderProps = {type: 'default'},
  onPress,
  touchableProps = {},
}: ButtonProps) => {
  const borderColor = disabled
    ? color.disableBorderColor
    : containerStyle?.borderColor ?? color.uclaBlue;
  const borderWidth = containerStyle?.borderWidth ?? 1;
  const loaderType = loaderProps.type;
  const imageType = imageProps?.type ?? undefined;
  const imageLeftRight = imageType === 'left' || imageType === 'right';
  const imageOpacity = disabled ? 0.5 : 1;
  const loadSize =
    loaderProps?.activityIndicator?.size ?? sizes.buttonLoaderSize;
  backgroundColor = disabled
    ? color.disableBackgroundColor
    : backgroundColor ?? containerStyle?.backgroundColor ?? color.uclaBlue;

  const mainContainerStyle = useMemo(() => {
    return {
      ...styles.containerStyle,
      backgroundColor: backgroundColor,
      borderColor: borderColor,
      borderWidth: borderWidth,
    };
  }, [backgroundColor, borderColor, borderWidth]);

  const mainSubContainerStyle = useMemo(() => {
    return {
      ...commonStyle.flex,
      ...(imageType === 'swipeLeft' || imageType === undefined
        ? commonStyle.flexRow
        : imageType === 'swipeRight'
        ? commonStyle.flexRowReverse
        : {}),
      marginHorizontal:
        imageProps.Source && !imageLeftRight ? normalizeValue(loadSize) : 5,
    };
  }, [imageType, imageProps.Source, imageLeftRight, loadSize]);

  const imageViewStyle = React.useMemo(() => {
    return imageType === 'left'
      ? styles.imageLeftStyle
      : imageType === 'right'
      ? styles.imageRightStyle
      : {};
  }, [imageType]);

  const mainImageViewStyle = useMemo(() => {
    return {
      ...imageViewStyle,
      marginLeft: loaderShow && imageType === 'swipeLeft' ? 5 : 0,
      marginRight: loaderShow && imageType === 'swipeRight' ? 5 : 0,
    };
  }, [loaderShow, imageType, imageViewStyle]);

  const mainTitleTextViewStyle = useMemo(() => {
    const styleObj = {
      marginHorizontal:
        imageProps.Source && imageLeftRight
          ? normalizeValue(loadSize * 3.2)
          : 5,
      ...commonStyle.alignItemCenter,
    };
    return imageLeftRight ? {...commonStyle.flex, ...styleObj} : {...styleObj};
  }, [imageLeftRight, imageProps.Source, loadSize]);

  const disableStyle = React.useMemo(() => {
    return {
      disableTextColor: disabled ? {color: color.transparentSemi} : {},
      disableButtonStyle: disabled
        ? {backgroundColor: backgroundColor, borderColor: borderColor}
        : {},
    };
  }, [disabled, backgroundColor, borderColor]);

  const loaderMainViewStyle = useMemo(() => {
    return loaderType === 'transparent'
      ? {...styles.loaderTransparentViewStyle}
      : loaderType === undefined
      ? {}
      : loaderType === 'left' || loaderType === 'swipeLeft'
      ? {
          ...styles.loaderAbsoluteStyle,
          left:
            loaderType === 'swipeLeft'
              ? -normalizeValue(loadSize)
              : (imageLeftRight ? -normalizeValue(loadSize) : 10) + 5,
        }
      : loaderType === 'right' || loaderType === 'swipeRight'
      ? {
          ...styles.loaderAbsoluteStyle,
          right:
            loaderType === 'swipeRight'
              ? -normalizeValue(loadSize)
              : (imageLeftRight ? -normalizeValue(loadSize) : 10) + 5,
        }
      : {};
  }, [loaderType, imageLeftRight, loadSize]);

  const LoaderView = () => {
    const activityIndicatorProps = useMemo(() => {
      return {
        size: normalizeValue(30),
        color: color.white,
        count: 12,
      };
    }, []);

    return (
      <View style={[loaderMainViewStyle, styles.loaderViewStyle]}>
        <LoaderComponent
          activityIndicatorType={'UIActivityIndicator'}
          {...loaderProps}
          activityIndicator={{
            ...activityIndicatorProps,
            ...loaderProps.activityIndicator,
          }}
        />
      </View>
    );
  };

  return (
    <TouchableOpacity
      {...getTestID(testIds.button)}
      disabled={disabled || loaderShow}
      onPress={onPress}
      {...touchableProps}>
      <View
        style={[
          mainContainerStyle,
          containerStyle,
          disableStyle.disableButtonStyle,
        ]}>
        {loaderShow && !imageLeftRight && loaderType === 'left'
          ? LoaderView()
          : null}

        <View
          style={[
            styles.subContainerStyle,
            !imageLeftRight ? commonStyle.alignItemCenter : {},
          ]}>
          {loaderShow && loaderType === 'default' ? (
            LoaderView()
          ) : (
            <View style={[mainSubContainerStyle, {}]}>
              {loaderShow && !imageLeftRight && loaderType === 'swipeLeft'
                ? LoaderView()
                : null}

              {imageProps?.Source ? (
                <View
                  style={[
                    styles.imageViewStyle,
                    mainImageViewStyle,
                    imageViewStyle,
                    {},
                  ]}>
                  <Image
                    source={imageProps?.Source}
                    style={[
                      styles.imageStyle,
                      imageProps.Style,
                      {opacity: imageOpacity},
                    ]}
                  />
                </View>
              ) : null}

              {titleText ? (
                <View
                  style={[
                    styles.titleTextViewStyle,
                    titleTextViewStyle,
                    mainTitleTextViewStyle,
                    {},
                  ]}>
                  <View>
                    {loaderShow &&
                    imageLeftRight &&
                    loaderType !== 'default' &&
                    loaderType !== 'transparent'
                      ? LoaderView()
                      : null}
                    <Text
                      style={[
                        styles.mainTitleTextStyle,
                        titleTextStyle,
                        disableStyle.disableTextColor,
                      ]}>
                      {titleText}
                    </Text>
                  </View>
                </View>
              ) : null}

              {loaderShow && !imageLeftRight && loaderType === 'swipeRight'
                ? LoaderView()
                : null}
            </View>
          )}
        </View>

        {loaderShow &&
        (loaderType === 'transparent' ||
          (!imageLeftRight && loaderType === 'right'))
          ? LoaderView()
          : null}
      </View>
    </TouchableOpacity>
  );
};

export default memo(ButtonComponent);

const styles = StyleSheet.create({
  containerStyle: {
    height: normalizeValue(50),
    borderRadius: normalizeValue(10),
    margin: normalizeValue(10),
    overflow: 'hidden',
  },
  subContainerStyle: {
    ...commonStyle.flex,
    marginHorizontal: normalizeValue(10),
    marginVertical: 5,
  },
  imageViewStyle: {
    ...commonStyle.justifyCenter,
    minWidth: normalizeValue(sizes.buttonImageSize),
    height: '100%',
  },
  imageStyle: {
    ...commonStyle.heightWidth100Percent,
  },
  imageLeftStyle: {
    position: 'absolute',
    left: 0,
  },
  imageRightStyle: {
    position: 'absolute',
    right: 0,
  },
  titleTextViewStyle: {
    ...commonStyle.justifyCenter,
    marginHorizontal: 4,
  },
  mainTitleTextStyle: {
    fontSize: normalizeValue(18),
    textAlign: 'center',
    color: color.white,
    fontWeight: 'bold',
  },
  loaderAbsoluteStyle: {
    position: 'absolute',
    top: 0,
    bottom: 0,
  },
  loaderViewStyle: {
    ...commonStyle.flex,
    ...commonStyle.justifyCenter,
    ...commonStyle.alignItemCenter,
  },
  loaderTransparentViewStyle: {
    ...StyleSheet.absoluteFillObject,
    ...commonStyle.justifyCenter,
    backgroundColor: color.transparentSemi,
  },
});
