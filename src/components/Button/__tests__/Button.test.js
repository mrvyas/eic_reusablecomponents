import React from 'react';
import renderer from 'react-test-renderer';
import ButtonComponent from '..';

describe('Button', () => {
  const imageURL =
    'https://cdn0.iconfinder.com/data/icons/yooicons_set01_socialbookmarks/512/social_google_box_white.png';

  test('default with text', () => {
    const wrapper = renderer.create(<ButtonComponent titleText="Button" />);
    expect(wrapper).toMatchSnapshot();
  });

  test('default disable', () => {
    const wrapper = renderer.create(
      <ButtonComponent titleText="Button" disabled={true} />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('default with out param', () => {
    const wrapper = renderer.create(<ButtonComponent />);
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image and loader', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'swipeLeft'}}
        loaderShow={true}
        loaderProps={{type: 'default'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image and loader swipe-right', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'swipeRight'}}
        loaderShow={true}
        loaderProps={{type: 'swipeRight'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image and loader swipe-left', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'swipeLeft'}}
        loaderShow={true}
        loaderProps={{type: 'swipeLeft'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image and loader right', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'right'}}
        loaderShow={true}
        loaderProps={{type: 'right'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image and loader left', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'left'}}
        loaderShow={true}
        loaderProps={{type: 'left'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image swipe-left and loader left', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'swipeLeft'}}
        loaderShow={true}
        loaderProps={{type: 'left'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with Image swipe-right and loader left', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'swipeRight'}}
        loaderShow={true}
        loaderProps={{type: 'right'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with loader transparent', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'left'}}
        loaderShow={true}
        loaderProps={{type: 'transparent'}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('with loader undefined', () => {
    const wrapper = renderer.create(
      <ButtonComponent
        titleText="Button"
        imageProps={{Source: {uri: imageURL}, type: 'left'}}
        loaderShow={true}
        loaderProps={{type: undefined}}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
