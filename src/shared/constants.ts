const KEYBOARD_TYPES = {
  default: 'default',
  numberPad: 'number-pad',
  decimalpad: 'decimal-pad',
  numeric: 'numeric',
  emailAddress: 'email-address',
  phonePad: 'phone-pad',
  numbersAndPunctuation: 'numbers-and-punctuation',
};

const FONT_TYPES = {
  black: 'black',
  blackItalic: 'blackItalic',
  bold: 'bold',
  boldItalic: 'boldItalic',
  book: 'book',
  bookItalic: 'bookItalic',
  extraBold: 'extraBold',
  extraBoldItalic: 'extraBoldItalic',
  light: 'light',
  lightItalic: 'lightItalic',
  medium: 'medium',
  mediumItalic: 'mediumItalic',
};

const strings = {
  blankString: '',
  stringEmpty: 'Empty',
  buttonDone: 'Done',
  buttonRetry: 'Retry',
  buttonClose: 'Close',
  buttonCancel: 'Cancel',
  radioButton: {
    titleDefaultText: 'Title Text',
  },
  modalContainer: {
    successTitle: 'Congratulations',
    successSubTitle: 'Your data is successfully updated',
    errorTitle: 'Uh, no!',
    errorSubTitle: 'Please check data properly',
    failureTitle: 'Sorry !',
    failureSubTitle: 'Given data is not proper, please check and try again',
    warningTitle: 'Attention !',
    warningSubTitle: 'Please fill all the given data field',
    networkTitle: 'Oops!... Network error',
    networkSubTitle: 'Please check your internet connection and try again',
  },
};

export {KEYBOARD_TYPES, FONT_TYPES, strings};
