import {Dimensions} from 'react-native';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

export const scaleWidth = SCREEN_WIDTH / 375;

export const scaleHeight = SCREEN_HEIGHT / 812;

const sizes = {
  buttonImageSize: 30,
  buttonLoaderSize: 20,
  scaleWidth,
  scaleHeight,
  scale: Math.min(scaleWidth, scaleHeight),
};

export default sizes;
