import {Platform} from 'react-native';

export const getTestID = (id: any) => {
  return Platform.OS === 'android'
    ? {accessible: true, accessibilityLabel: id}
    : {testID: id};
};

export const testIds = {
  basicLoader: 'basicLoader',
  ballIndicator: 'ballIndicator',
  materialIndicator: 'materialIndicator',
  skypeIndicator: 'skypeIndicator',
  uiActivityIndicator: 'uiActivityIndicator',
  radioButton: 'radioButton',
  radioButtonList: 'radioButtonList',
  toggleButton: 'toggleButton',
  checkBox: 'checkBox',
  checkBoxList: 'checkBoxList',
  button: 'button',
  buttonGroup: 'buttonGroup',
  buttonGroupItem: 'buttonGroupItem',
  textInput: 'textInput',
  subTextInput: 'subTextInput',
  dropdown: 'dropdown',
  dropdownSelect: 'dropdownSelect',
  modalComponent: 'modalComponent',
  modalComponentCloseButton: 'modalComponentCloseButton',
  header: 'header',
  cardComponent: 'cardComponent',
  collapsibleCardComponent: 'collapsibleCardComponent',
  listItem: 'listItem',
  searchBar: 'searchBar',
  searchInput: 'searchInput',
  leftRightImageTestId: 'leftRightImageTestId',
  chip: 'chip',
  chipImage: 'chipImage',
};
