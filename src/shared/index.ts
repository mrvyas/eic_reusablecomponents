import color from './color';
import commonStyle from './styles';
import sizes from './sizes';
import {KEYBOARD_TYPES, FONT_TYPES, strings} from './constants';
import {testIds, getTestID} from './testIds';

export {
  color,
  sizes,
  commonStyle,
  KEYBOARD_TYPES,
  FONT_TYPES,
  strings,
  getTestID,
  testIds,
};
