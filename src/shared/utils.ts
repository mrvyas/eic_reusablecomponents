import sizes from './sizes';

export function normalizeValue(sizeValue: number) {
  const newSize = Math.ceil(sizeValue * sizes.scale);
  return newSize;
}
