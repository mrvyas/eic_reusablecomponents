import {StyleSheet} from 'react-native';
import color from './color';
import {normalizeValue} from './utils';

const commonStyle = StyleSheet.create({
  flex: {flex: 1},
  flex2: {flex: 2},
  flex3: {flex: 3},
  flexRow: {flexDirection: 'row'},
  flexRowReverse: {flexDirection: 'row-reverse'},

  justifyCenter: {justifyContent: 'center'},
  justifyFlexStart: {justifyContent: 'flex-start'},
  justifyFlexEnd: {justifyContent: 'flex-end'},

  alignItemCenter: {alignItems: 'center'},

  alignSelfCenter: {alignSelf: 'center'},
  alignSelfFlexStart: {alignSelf: 'flex-start'},
  alignSelfFlexEnd: {alignSelf: 'flex-end'},

  overFlowHidden: {overflow: 'hidden'},

  heightWidth100Percent: {height: '100%', width: '100%'},

  storyBookHeaderText: {
    fontSize: normalizeValue(15),
    fontWeight: 'bold',
    marginBottom: normalizeValue(10),
  },
  storyBookHeaderView: {
    marginTop: normalizeValue(15),
  },
  textColorBlack: {
    color: color.black,
  },
});

export default commonStyle;
