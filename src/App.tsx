/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useMemo} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import ButtonComponent from './components/Button';
import LoaderComponent from './components/Loader';
import {color} from './shared';

const App = () => {
  const activityIndicatorProps = useMemo(() => {
    return {
      size: 30,
      color: color.grey,
      count: 12,
    };
  }, []);

  return (
    <SafeAreaView style={styles.mainContainer}>
      <ScrollView
        contentContainerStyle={styles.container}
        contentInsetAdjustmentBehavior="automatic">
        <View>
          <Text style={styles.textStyle}>
            Edit App.tsx To change this screen and then come back to see your
            edits.
          </Text>
          <ButtonComponent
            titleText="Default"
            containerStyle={styles.containerStyle}
            titleTextStyle={styles.titleTextStyle}
          />
          <LoaderComponent
            activityIndicatorType={'UIActivityIndicator'}
            activityIndicator={activityIndicatorProps}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 15,
  },
  textStyle: {
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
  containerStyle: {
    height: 50,
    backgroundColor: color.uclaBlue,
    borderColor: color.uclaBlue,
  },
  titleTextStyle: {
    fontSize: 22,
    color: color.white,
    fontWeight: 'bold',
  },
});

export default App;
